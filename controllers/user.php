<?php
namespace rest\controllers;
use rest\dao\user as user_dao;
use rest\system\ObjectOperations;
class user{
    private $_dao; 
    public function __construct() {
       
        $this->_dao = new user_dao();
    
    }
    
    public function add()
	{

            global $request;
        //    print_r($request->getRequestParams());die;
           $response =  $this->_dao->insertUserData($request->getRequestParams());
           echo ObjectOperations::json_encode(ObjectOperations::objectToArray($response));
            exit;
          // print_r($response);die();
	}
        
        public function get()
	{

            global $request;
        //    print_r($request->getRequestParams());die;
           $response =  $this->_dao->getUserData($request->getRequestParams());
           echo ObjectOperations::json_encode(ObjectOperations::objectToArray($response));
            exit;
          // print_r($response);die();
	}
}
