<?php
namespace rest\controllers;
use rest\dao\products as products_dao;
use rest\system\ObjectOperations;
class products
{
    private $_dao; 
    public function __construct() {
        $this->_dao = new products_dao();    
    
    }
	public function add()
	{
            global $request;
           $response =  $this->_dao->insertProductData($request->getRequestParams());
           echo ObjectOperations::json_encode(ObjectOperations::objectToArray($response));
           exit;
	}
	public function delete()
	{
	   global $request;
           $response =  $this->_dao->deleteProductData($request->getRequestParams());
           echo ObjectOperations::json_encode(ObjectOperations::objectToArray($response));
           exit;
    }
    public function get() {
           global $request;
           $response =  $this->_dao->getProductData($request->getRequestParams());
           echo ObjectOperations::json_encode(ObjectOperations::objectToArray($response));
           exit;
    
    }
    public function update()
	{
	   global $request;
           $response =  $this->_dao->updateProductData($request->getRequestParams());
           echo ObjectOperations::json_encode(ObjectOperations::objectToArray($response));
           exit;
    }


}
