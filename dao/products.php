<?php
namespace rest\dao;
use rest\dao\dao;
class products extends dao {
   public function __construct() {
      parent::__construct();
   }
    public function getProductData($get = array()) {
        if(!empty($get["query_string"])){
        $query = 'Select product_id , product_name, quantity , price , status from product where product_name LIKE "%'.$get["query_string"].'%"';
        $result= $this->select($query,TRUE);
        if($result){
            return array('status'=>"success",'data'=>$result); 
        }
        else{
            return array('status'=>"failed",'message'=>"Product Not Found.");
        }
        }
        else{
            return array('status'=>"failed",'message'=>"Incomplete Data");
        }
    
    }
    
    public function insertProductData($get = array()) {
        
        if(!empty($get["product_name"]) && !empty($get["quantity"]) && !empty($get["price"]) ){
            
            $query = 'Select product_id , product_name, quantity , price , status from users where product_name="'.$get["product_name"].'"';
            $result= $this->select($query,TRUE);
            if($result){
                return array('status'=>"failed",'message'=>"Product already Exist.");
            } 
    
            $query = 'Insert into product (product_name,quantity,price) values("'.$get['product_name'].'","'.$get['quantity'].'","'.$get['price'].'")' ;
         //   echo $query;die;
            $result= $this->insert($query);
            if($result){
                return array('status'=>"success",'message'=>"Product Added Successfully.");
            }
            else{
                return array('status'=>"failed",'message'=>"InCorrect Data");
            }
    
       }
       else{
            return array('status'=>"failed",'message'=>"Incomplete Data");
        }
    }
    
    
    
    public function deleteProductData($get = array()) {
        
        if(!empty($get["product_id"])){
            
    
            $query = 'UPDATE product SET status = 0 where product_id = "'.$get["product_id"].'"' ;
         //   echo $query;die;
            $result= $this->update($query);
            if($result){
                return array('status'=>"success",'message'=>"Product deleted Successfully.");
            }
            else{
                return array('status'=>"failed",'message'=>"Product Not Found");
            }
    
       }
       else{
            return array('status'=>"failed",'message'=>"Incomplete Data");
        }
    }
    
    public function updateProductData($get = array()) {
        
        if(!empty($get["product_id"]) && !empty($get["product_name"]) && !empty($get["quantity"]) && !empty($get["price"])){
                
            $query = 'UPDATE product SET status = 1 , product_name = "'.$get["product_name"].'" ,quantity = "'.$get["quantity"].'" ,price = "'.$get["price"].'" where product_id = "'.$get["product_id"].'"' ;
          //  echo $query;die;
            $result= $this->update($query);
            if($result){
                return array('status'=>"success",'message'=>"Product updated Successfully.");
            }
            else{
                return array('status'=>"failed",'message'=>"Product Not Found");
            }
    
       }
       else{
            return array('status'=>"failed",'message'=>"Incomplete Data");
        }
    }
    
}
