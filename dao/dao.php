<?php
namespace rest\dao;

use rest\system\MemcachedUtilities;
class dao{
    private static $_conn;
    public function __construct() {
        global $_config;
        if(!self::$_conn){
            self::$_conn = new \mysqli($_config['db']['host'], $_config['db']['username'],$_config['db']['password'],$_config['db']['database']);
            if(!self::$_conn){
                throw new Exception("Error in connecting to db");
            }
        }
    }
    public function query($query) {
        $conn = self::$_conn;
        $conn->query($query);
    }
    public function select($query,$cache = FALSE) {
        $conn = self::$_conn;
        global $_config;
        if($cache && $_config['memcache']){
            $memcacheObj=new MemcachedUtilities();
            $key=md5($query);
            $result=$memcacheObj->get($key);
            if(empty($result)){    

            $result = $conn->query($query);
            $memcacheObj->set($key,$result);
            //  print_r($result);die;
            }
        }
        else{
            $result = $conn->query($query);
        }
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                $final_result[] = $row; 
            }
        } 
        
        return !empty($final_result)?$final_result:array();
    }
    public function insert($query) {
        
        $conn = self::$_conn;
        //print_r($query);die;
        $result = $conn->query($query);
      //  print_r($conn->insert_id);die;
        
        if($result) {
            return $conn->insert_id;   
        }
        return null;
    }
    public function update($query) {
        $conn = self::$_conn;
        $conn->query($query);   
        return $conn->affected_rows;
       
    }
    public function del($query) {
        $conn = self::$_conn;
        $conn->query($query);              
        return $conn->affected_rows;       
    }
}
