<?php
namespace rest\dao;
use rest\dao\dao;
class user extends dao {
    
    const HASH_FUNCTION_SHA256  = 'sha256';
    
   public function __construct() {
      parent::__construct();
      global $_config;
      $this->_api_key = $_config['api_key'];
      $this->algo=self::HASH_FUNCTION_SHA256;
   }
    public function getUserData($get = array()) {
        
       if(!empty($get["email_id"]) && !empty($get["password"]) ){
        $query = 'Select user_id , password , email_id from users where email_id="'.$get["email_id"].'" and password = "'.md5($get["password"]).'"';
    //    print_r($query);die;
        $result= $this->select($query,TRUE);
       // print_r($result);die;
        if($result){
            $user_id = $result[0]["user_id"];
            $password = md5($get["password"]);
            $key = $this->_api_key.$user_id.$password;
            $x_token = hash($this->algo,$key);
            return array('status'=>"success",'token'=>$x_token,'user_id'=>$user_id);
        }
        else{
            return array('status'=>"failed",'message'=>"Invalid Data");
        }
       }
       else{
           return array('status'=>"failed",'message'=>"Incomplete Data");
       }
    }
    
    
    public function getUserDataById($id) {
        
        $query = 'Select password from users where user_id="'.$id.'"';
    //    print_r($query);die;
        $result= $this->select($query,TRUE);
       // print_r($result);die;
        if($result){
            $user_id = $id;
            $password = $result[0]['password'];
            $key = $this->_api_key.$user_id.$password;
            $x_token = hash($this->algo,$key);
         //   print_r($user_id."======".$password."============".$this->_api_key."========".$x_token);die;
            return $x_token;
        }
        else{
            return 0;
        }
    }
    
    
    public function insertUserData($get = array()) {
        
        if(!empty($get["email_id"]) && !empty($get["password"]) ){
            
            if (!filter_var($get["email_id"], FILTER_VALIDATE_EMAIL)) {
                return array('status'=>"failed",'message'=>"Invalid EmailId");
            }
            
            $query = 'Select user_id , password , email_id from users where email_id="'.$get["email_id"].'"';
            $result= $this->select($query);
            if($result){
                return array('status'=>"failed",'message'=>"Email already Registered.");
            }   
            $query = 'Insert into users (password,email_id) values("'.md5($get['password']).'","'.$get['email_id'].'")' ;
           // echo $query;die;
            $result= $this->insert($query);
            if($result){
                $user_id = $result;
                $password = md5($get['password']);
                $key = $this->_api_key.$user_id.$password;
                $x_token = hash($this->algo,$key);
                return array('status'=>"success",'token'=>$x_token,'user_id'=>$user_id);
            }
            else{
                return array('status'=>"failed",'message'=>"InCorrect Data");
            }
        }
        else{
            return array('status'=>"failed",'message'=>"Incomplete Data");
        }
    }
    


}
