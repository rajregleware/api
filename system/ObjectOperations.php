<?php

namespace rest\system;
/**
 * This class is used for all object operations
 * @class ObjectOperations
 * @version 1.0
 * @category Utility
 * @package system/util
 * @author Raj Choudhary>
 */
class ObjectOperations {

    /**
     * This method is used to get object item counts
     * @method getCount
     * @access public
     * @param object $object
     * @return int
     */
    public static function getCount($object) {
        $count = 0;
        foreach ($object as $value) {
            $count++;
        }
        return $count;
    }

    /**
     * This method is used to convert array in to object
     * @method arrayToObject
     * @access public
     * @param array $array
     * @return object standard class object
     */
    public static function arrayToObject(array $array) {
        if (!empty($array)) {
            $object = new \stdClass();
            foreach ($array as $key => $val) {
                if (is_array($val)) {
                    $val = self::arrayToObject($val);
                }
                $object->{$key} = $val;
            }
            return $object;
        } else {
            return null;
        }
    }

    /**
     * This method is used to convert object to array
     * @method objectToArray
     * @access public
     * @param object $object
     * @return array
     */
    public static function objectToArray($object) {
        return json_decode(self::json_encode($object), true);
    }

    /**
     * 
     * @param object $object
     * @return string json encoded string
     * @throws UtilException
     */
    public static function json_encode($object) {
        $encoded_string = json_encode($object);
        $error_code = json_last_error();
        if ($error_code == JSON_ERROR_NONE) {
            return $encoded_string;
        } else {
            throw new Exception("dfaddaf");
        }
    }

}
