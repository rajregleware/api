<?php

namespace rest\system;
use rest\system\user;
use rest\system\ObjectOperations;
class ApiAuth {
    const HASH_FUNCTION_SHA256  = 'sha256';
    
    /**
     * class constructor
     * @method __construct
     * @access public
     */
    public function __construct() {
        global $request;
        $this->_request = $request->getRequestParams();
    }
    /**
     * 
     * @return boolean true if token authenticated successfully
     * @throws AuthException
     */
    public function validate(){
        if(!empty($this->_request['token']) && !empty($this->_request['user_id'])){
        $token = $this->_request['token'];
        $user_id = $this->_request['user_id'];
        $user = new user($user_id);
        if($token == $user->_token) {
            return TRUE;   
        } else{
            $res = array('status'=>"failed",'message'=>"Token Mismatched.");
            echo ObjectOperations::json_encode(ObjectOperations::objectToArray($res));
            exit;
        }
        }
        else{
            $res = array('status'=>"failed",'message'=>"Token and userId required.");
            echo ObjectOperations::json_encode(ObjectOperations::objectToArray($res));
            exit;
        }
    }
}


