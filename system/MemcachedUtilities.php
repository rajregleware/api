<?php

namespace rest\system;

class MemcachedUtilities {

    public static $memcache = null;
    private $connected = 1;

    public function __construct() {
        
        global $_config;

        if (null === MemcacheUtilities::$memcache) {
            MemcacheUtilities::$memcache = new memcached();
            MemcacheUtilities::$memcache->addServers($_config['memcache_server']);
            if (!isset(MemcacheUtilities::$memcache))
                $this->connected == 1;
            return MemcacheUtilities::$memcache;
        }
    }

    public function set($key, $data, $expire) {

        if ($this->connected == 0)
            return false;
    $result = MemcacheUtilities::$memcache->set($key, $data, (!empty($expire)) ? $expire : $_config['memcache_default_lifetime']);
        return $result;
    }

    public function get($key) {
        if ($this->connected == 0)
            return null;
        $result = MemcacheUtilities::$memcache->get($key);
        return $result;
    }

    public function delete($key, $time = 0) {
        if ($this->connected == 0)
            return null;
        $result = MemcacheUtilities::$memcache->delete($key, $time);
        return $result;
    }

    public function setMulti($keyValueArray, $expire) {

        if ($this->connected == 0)
            return false;
        $result = MemcacheUtilities::$memcache->setMulti($keyValueArray, (!empty($expire)) ? $expire : ScConstants::$memCacheDefaultLifeTime);
        return $result;
    }

    public function getMulti($keys, $cas_tokens, $flags) {
        if ($this->connected == 0)
            return null;
        $result = MemcacheUtilities::$memcache->getMulti($keys, $cas_tokens, $flags);
        return $result;
    }

    public function deleteMulti($keysArray, $time = 0) {
        if ($this->connected == 0)
            return null;
        $result = MemcacheUtilities::$memcache->deleteMulti($keysArray, $time);
        return $result;
    }

    public function getAllKeys() {
        if ($this->connected == 0)
            return null;
        $result = MemcacheUtilities::$memcache->getAllKeys();
        return $result;
    }

    public function append($key, $value) {
        if ($this->connected == 0)
            return null;
        $result = MemcacheUtilities::$memcache->append($key, $value);
        return $result;
    }

    public function prepend($key, $value) {
        if ($this->connected == 0)
            return null;
        $result = MemcacheUtilities::$memcache->prepend($key, $value);
        return $result;
    }

    public function incrementItemValue($key, $counter, $initial_value = 0, $expiry = 0) {
        if ($this->connected == 0)
            return false;
        $result = MemcacheUtilities::$memcache->increment($key, $counter, $initial_value, $expiry);
        return $result;
    }

    public function decrementItemValue($key, $counter, $initial_value = 0, $expiry = 0) {
        if ($this->connected == 0)
            return false;
        $result = MemcacheUtilities::$memcache->decrement($key, $counter, $initial_value, $expiry);
        return $result;
    }

    public function checkMemCacheStatus() {
        return (bool) $this->connected;
    }

    //Set a new expiration on an item
    public function touch($key, $newexpiry) {
        if ($this->connected == 0)
            return false;
        $result = MemcacheUtilities::$memcache->touch($key, $newexpiry);
        return $result;
    }

    public function flushMemcache($delay = 0) {
        if ($this->connected == 0)
            return false;
        $result = MemcacheUtilities::$memcache->flush($delay);
        return $result;
    }

    // Return the result code of the last operation
    public function getResultCode() {
        if ($this->connected == 0)
            return false;
        $result = MemcacheUtilities::$memcache->getResultCode();
        return $result;
    }

    public function getResultMessage() {
        if ($this->connected == 0)
            return false;
        $result = MemcacheUtilities::$memcache->getResultMessage();
        return $result;
    }

    public function getServerList() {
        if ($this->connected == 0)
            return false;
        $result = MemcacheUtilities::$memcache->getServerList();
        return $result;
    }

}
